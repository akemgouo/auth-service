package com.sid.authservice.validator;

import com.sid.authservice.web.dto.RolesDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

public class RolesValidator {

    public static Collection<String> validate(final RolesDto rolesDto) {

        Collection<String> errors = new ArrayList<>();

        if (!StringUtils.hasLength(rolesDto.getRoleName()) || rolesDto == null)
            errors.add("Le roles est obligatoire");
        return errors;
    }
}
