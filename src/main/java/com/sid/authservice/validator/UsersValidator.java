package com.sid.authservice.validator;

import com.sid.authservice.web.dto.UsersDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Pattern;

public class UsersValidator {
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern PATTERN = Pattern.compile(EMAIL_PATTERN);

    public static Collection<String> validate(final UsersDto usersDto) {

        Collection<String> errors = new ArrayList<>();

        if (usersDto == null) {
            errors.add("Le username de l'utilisateur est obligatoire");
            errors.add("Le mot de passe de passe de l'utilisateur est obligatoire");
            errors.add("Le nom de l'utilisateur est obligatoire");
            return errors;
        }

        if (!StringUtils.hasLength(usersDto.getUsername())) {
            errors.add("Le username de l'utilisateur est obligatoire");
        }
        if (!StringUtils.hasLength(usersDto.getFullname())) {
            errors.add("Le nom de l'utilisateur est obligatoire");
        }
        if (!StringUtils.hasLength(usersDto.getPassword())) {
            errors.add("Le mot de passe de l'utilisateur est obligatoire");
        }

        return errors;
    }
}
