package com.sid.authservice.web.mappers;


import com.sid.authservice.domain.Roles;
import com.sid.authservice.web.dto.RolesDto;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class RolesMapperDecorator implements RolesMapper {

    private RolesMapper rolesMapper;

    @Autowired
    public void setUsersMapper(RolesMapper rolesMapper) {
        this.rolesMapper = rolesMapper;
    }

    @Override
    public RolesDto convertEntityToDto(final Roles roles) {
        return rolesMapper.convertEntityToDto(roles);
    }

    @Override
    public Roles convertDtoToEntity(final RolesDto rolesDto) {
        return rolesMapper.convertDtoToEntity(rolesDto);
    }
}
