package com.sid.authservice.web.mappers;

import com.sid.authservice.domain.Roles;
import com.sid.authservice.web.dto.RolesDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = DateMapper.class)
@DecoratedWith(value = RolesMapperDecorator.class)
public interface RolesMapper {

    @Mapping(source = "roles.id", target = "roleId")
    @Mapping(source = "roles.lastModifiedDate", target = "lastModifiedDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    @Mapping(source = "roles.createdDate", target = "createdDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    RolesDto convertEntityToDto(final Roles roles);

    @Mapping(source = "rolesDto.roleId", target = "id")
    @Mapping(source = "rolesDto.lastModifiedDate", target = "lastModifiedDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    @Mapping(source = "rolesDto.createdDate", target = "createdDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    Roles convertDtoToEntity(final RolesDto rolesDto);
}
