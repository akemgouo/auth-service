package com.sid.authservice.web.mappers;

import com.sid.authservice.domain.Users;
import com.sid.authservice.web.dto.UsersDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = DateMapper.class)
@DecoratedWith(value = UsersMapperDecorator.class)
public interface UsersMapper {

    @Mapping(source = "users.id", target = "userId")
    @Mapping(source = "users.lastModifiedDate", target = "lastModifiedDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    @Mapping(source = "users.createdDate", target = "createdDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    UsersDto convertEntityToDto(final Users users);

    @Mapping(source = "usersDto.userId", target = "id")
    @Mapping(source = "usersDto.lastModifiedDate", target = "lastModifiedDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    @Mapping(source = "usersDto.createdDate", target = "createdDate"/*, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS*/)
    Users convertDtoToEntity(final UsersDto usersDto);
}
