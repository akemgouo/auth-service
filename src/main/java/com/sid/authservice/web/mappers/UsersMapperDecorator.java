package com.sid.authservice.web.mappers;


import com.sid.authservice.domain.Users;
import com.sid.authservice.web.dto.UsersDto;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class UsersMapperDecorator implements UsersMapper {

    private UsersMapper usersMapper;

    @Autowired
    public void setUsersMapper(UsersMapper usersMapper) {
        this.usersMapper = usersMapper;
    }

    @Override
    public UsersDto convertEntityToDto(final Users users) {
        return usersMapper.convertEntityToDto(users);
    }

    @Override
    public Users convertDtoToEntity(final UsersDto usersDto) {
        return usersMapper.convertDtoToEntity(usersDto);
    }
}
