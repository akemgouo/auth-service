package com.sid.authservice.web.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class ChangePasswordUsersDto {

    private UUID id;

    private String password;

    private String confirmpassword;
}
