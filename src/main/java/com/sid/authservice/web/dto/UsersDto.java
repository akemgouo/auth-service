package com.sid.authservice.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sid.authservice.domain.Roles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Null;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersDto {

    @Builder
    public UsersDto(UUID userId, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate, String password, String fullname, String username, String isAdmin, String isOauthAccount, String deletedFlag, OffsetDateTime credentialsExpiryDate, boolean isAccountExpired, boolean isAccountLocked, Collection<Roles> roles) {
        this.userId = userId;
        this.createdDate = createdDate;
        this.password = password;
        this.fullname = fullname;
        this.username = username;
        this.isAdmin = isAdmin;
        this.isOauthAccount = isOauthAccount;
        this.deletedFlag = deletedFlag;
        this.credentialsExpiryDate = credentialsExpiryDate;
        this.isAccountExpired = isAccountExpired;
        this.isAccountLocked = isAccountLocked;
        this.lastModifiedDate = lastModifiedDate;
        this.roles = roles;
    }

    @Null
    private UUID userId;

    @Null
    private Long version;


    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime createdDate;

    @Null
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime lastModifiedDate;

    private String fullname;

    private String username;

    private String password;

    private String isAdmin = "N";;

    private String isOauthAccount = "N";

    private String deletedFlag;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime credentialsExpiryDate;

    private boolean isAccountExpired;

    private boolean isAccountLocked;

    Collection<Roles> roles = new HashSet<>();
}
