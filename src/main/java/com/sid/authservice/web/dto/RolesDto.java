package com.sid.authservice.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Null;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesDto {

    @Builder
    public RolesDto(UUID roleId, OffsetDateTime createdDate, String roleName, OffsetDateTime lastModifiedDate) {
        this.roleId = roleId;
        this.createdDate = createdDate;
        this.roleName = roleName;
        this.lastModifiedDate = lastModifiedDate;
    }

    @Null
    private UUID roleId;

    @Null
    private Long version;

    @Null
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime createdDate;

    @Null
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime lastModifiedDate;

    private String roleName;

}
