package com.sid.authservice.services;

import com.sid.authservice.exceptions.ErrorCodes;
import com.sid.authservice.exceptions.InvalidOperationException;
import com.sid.authservice.domain.Roles;
import com.sid.authservice.domain.Users;
import com.sid.authservice.repositories.RolesRepository;
import com.sid.authservice.repositories.UsersRepository;

import com.sid.authservice.web.dto.ChangePasswordUsersDto;
import com.sid.authservice.web.dto.UsersDto;
import com.sid.authservice.web.mappers.UsersMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final UsersMapper usersMapper;
    private final RolesRepository rolesRepository;

    @Override
    public UsersDto save(final UsersDto usersDto) {
        return usersMapper.convertEntityToDto(usersRepository.save(usersMapper.convertDtoToEntity(usersDto)));
    }

    @Override
    public void addRoleToUser(final String username, final String roleName) {

        Optional<Users> users = usersRepository.findByUsername(username);
        Optional<Roles> roles = rolesRepository.findByRoleName(roleName);
        if (users.isPresent() && roles.isPresent()) {
            users.get().getRoles().add(roles.get());
        }
    }

    @Override
    public UsersDto loadUserByUsername(final String username) {
        return usersRepository.findByUsername(username)
                .map(usersMapper::convertEntityToDto).orElseThrow(
                        () -> new EntityNotFoundException("L'utilisateur fourni avec le username = " + username + " est inexistant")
                );
    }

    @Override
    public List<UsersDto> findAll() {
        return usersRepository.findAll()
                .stream()
                .map(usersMapper::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UsersDto changePasswordUser(final ChangePasswordUsersDto dto) {
        validateDtoUser(dto);
        Optional<Users> utilisateurOptional = usersRepository.findById(dto.getId());
        if (!utilisateurOptional.isPresent()) {
            log.warn("Aucun utilisateur n'a été trouvé avec l' ID = " + dto.getId());
            throw new com.sid.authservice.exceptions.EntityNotFoundException("Aucun utilisateur n'a été trouvé avec l' ID = " + dto.getId(),
                    ErrorCodes.UTILISATEUR_NOT_FOUND);
        }
        Users utilisateur = utilisateurOptional.get();
        //utilisateur.setPassword(passwordEncoder.encode(dto.getPassword()));
        return usersMapper.convertEntityToDto(usersRepository.save(utilisateur));
    }

    private void validateDtoUser(final ChangePasswordUsersDto dto) {
        if (dto == null) {
            log.warn("Impossible de modifier le mot de passe avec un object NULL");
            throw new InvalidOperationException("Aucun information n'a été fourni pour changer le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }
        if (dto.getId() == null) {
            log.warn("Impossible de modifier le mot de passe avec un ID NULL");
            throw new InvalidOperationException("ID utilisateur NULL impossible de modifier le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }

        if (!StringUtils.hasLength(dto.getPassword()) || !StringUtils.hasLength(dto.getConfirmpassword())) {
            log.warn("Impossible de modifier le mot de passe avec un mot de passe NULL");
            throw new InvalidOperationException("Mot de passe utilisateur NULL impossible de modifier le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }

        if (!dto.getPassword().equals(dto.getConfirmpassword())) {
            log.warn("Impossible de modifier le mot de passe utilisateur avec deux mot de passe differents");
            throw new InvalidOperationException("Mot de passe utilisateur non conforme :: Impossible de modifier le mot de passe",
                    ErrorCodes.UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID);
        }
    }
}
