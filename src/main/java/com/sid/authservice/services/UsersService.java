package com.sid.authservice.services;

import com.sid.authservice.web.dto.ChangePasswordUsersDto;
import com.sid.authservice.web.dto.UsersDto;

import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.List;
import java.util.Optional;

public interface UsersService {

    UsersDto save(final UsersDto usersDto);
    void addRoleToUser(final String username, final String roleName);
    UsersDto loadUserByUsername(final String username);
    List<UsersDto> findAll();
    UsersDto changePasswordUser(final ChangePasswordUsersDto dto);
}
