package com.sid.authservice.services;

import com.sid.authservice.exceptions.EntityAlreadyExistException;
import com.sid.authservice.exceptions.EntityNotFoundException;
import com.sid.authservice.exceptions.ErrorCodes;
import com.sid.authservice.domain.Roles;
import com.sid.authservice.exceptions.InvalidEntityException;
import com.sid.authservice.repositories.RolesRepository;
import com.sid.authservice.validator.RolesValidator;
import com.sid.authservice.web.dto.RolesDto;
import com.sid.authservice.web.mappers.RolesMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RolesServiceImpl implements RolesService {

    private final RolesRepository rolesRepository;
    private final RolesMapper rolesMapper;

    @Override
    public RolesDto save(final RolesDto rolesDto) {
        if (rolesDto == null)
            throw new EntityNotFoundException("L'objet role est null ", ErrorCodes.ROLES_NOT_FOUND);
        Collection<String> errorsValidate = RolesValidator.validate(rolesDto);
        if (!errorsValidate.isEmpty()){
            log.error("Roles is not valid {}", rolesDto);
            throw new InvalidEntityException("Roles is not valid", ErrorCodes.ROLES_NOT_VALID, errorsValidate);
        }
        Optional<RolesDto> rolesDtoOptional = Optional.ofNullable(this.findByRoleName(rolesDto.getRoleName()));
        if (!rolesDtoOptional.isPresent()) {
            log.error("Roles provide {} exist in database ", rolesDto.getRoleName());
            throw new EntityAlreadyExistException("Roles provide exist in database", ErrorCodes.ROLES_ALREADY_EXISTS);
        }
        return rolesMapper.convertEntityToDto(rolesRepository.save(rolesMapper.convertDtoToEntity(rolesDto)));
    }

    @Override
    public void deleteRole(final UUID roleId) {
        if(roleId == null){
            log.warn("Role id inexistant");
        }
        Optional<Roles> role = rolesRepository.findById(roleId);
        if(!role.isPresent())
            throw new EntityNotFoundException("Le role n'existe pas en BD ",ErrorCodes.ROLES_NOT_FOUND);
        long nbUsers = role.get().getUsers().stream().count();
        if(nbUsers > 0L)
            throw new EntityNotFoundException("Il existe des utilisateurs ", ErrorCodes.UTILISATEUR_ALREADY_EXISTS);
        else
            rolesRepository.deleteById(roleId);
    }

    @Override
    public List<RolesDto> findAll() {
        return rolesRepository.findAll()
                .stream()
                .map(rolesMapper::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public RolesDto findByRoleName(String roleName) {
        return rolesRepository.findByRoleName(roleName)
                .map(rolesMapper::convertEntityToDto)
                .orElseThrow(
                () -> new EntityNotFoundException("Le role fourni avec le role = " + roleName + " est inexistant",
                        ErrorCodes.ROLES_NOT_FOUND)
        );
    }
}
