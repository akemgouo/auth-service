package com.sid.authservice.services;

import com.sid.authservice.web.dto.RolesDto;

import java.util.List;
import java.util.UUID;

public interface RolesService {

    RolesDto save(final RolesDto rolesDto);
    void deleteRole(final UUID roleId);
    List<RolesDto> findAll();
    RolesDto findByRoleName(final String roleName);
}
