package com.sid.authservice;

import com.sid.authservice.services.RolesService;
import com.sid.authservice.services.UsersService;
import com.sid.authservice.web.dto.RolesDto;
import com.sid.authservice.web.dto.UsersDto;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
@EnableDiscoveryClient
public class AuthServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(UsersService usersService, RolesService rolesService){
		return args -> {
			RolesDto r1 = rolesService.save(RolesDto.builder()
					.roleName("USER")
					.build());
			RolesDto r2 = rolesService.save(RolesDto.builder()
					.roleName("ADMIN")
					.build());
			rolesService.save(RolesDto.builder()
					.roleName("CUSTOMER_MANAGER")
					.build());
			rolesService.save(RolesDto.builder()
					.roleName("PRODUCT_MANAGER")
					.build());
			rolesService.save(RolesDto.builder()
					.roleName("BILL_MANAGER")
					.build());

			usersService.save(UsersDto.builder()
					.username("user1")
					.password("1234")
					.fullname("user1")
					//.isAdmin("N")
					//.isOauthAccount("N")
					.roles(new ArrayList<>())
					.build());

			usersService.save(UsersDto.builder()
					.username("admin")
					.password("1234")
					.fullname("admin")
					.isAdmin("Y")
					//.isOauthAccount("N")
					.roles(new ArrayList<>())
					.build());

			usersService.save(UsersDto.builder()
					.username("user2")
					.password("1234")
					.fullname("user2")
					//.isAdmin("N")
					//.isOauthAccount("N")
					.roles(new ArrayList<>())
					.build());

			usersService.save(UsersDto.builder()
					.username("user3")
					.password("1234")
					.fullname("user3")
					//.isAdmin("N")
					//.isOauthAccount("N")
					.roles(new ArrayList<>())
					.build());


			usersService.addRoleToUser("user1","USER");
			usersService.addRoleToUser("admin","ADMIN");
			usersService.addRoleToUser("admin","USER");
			usersService.addRoleToUser("user2","USER");
			usersService.addRoleToUser("user2","CUSTOMER_MANAGER");
			usersService.addRoleToUser("user3","USER");
			usersService.addRoleToUser("user3","PRODUCT_MANAGER");
		};
	}

}
