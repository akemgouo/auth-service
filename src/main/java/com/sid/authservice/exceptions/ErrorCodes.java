package com.sid.authservice.exceptions;

public enum ErrorCodes {

    BAD_CREDENTIAL(400),
    UNAUTHORIZED(401),
    CONFLICT(500),

    ROLES_NOT_FOUND(7000),
    ROLES_NOT_VALID(7001),
    ROLES_ALREADY_EXISTS(7002),

    PRIVILEGES_NOT_FOUND(8000),
    PRIVILEGES_NOT_VALID(8001),
    PRIVILEGES_ALREADY_EXISTS(8002),

    UTILISATEUR_NOT_FOUND(14000),
    UTILISATEUR_NOT_VALID(14001),
    UTILISATEUR_ALREADY_EXISTS(14002),
    UTILISATEUR_CHANGE_PASSWORD_OBJECT_NOT_VALID(14003);

    private int code;

    ErrorCodes(final int code) {
        this.code = code;
    }

    public final int getCode() {
        return code;
    }
}
