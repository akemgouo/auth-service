package com.sid.authservice.repositories;

import com.sid.authservice.domain.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RolesRepository extends JpaRepository<Roles, UUID> {

    Optional<Roles> findByRoleName(final String roleName);
}
